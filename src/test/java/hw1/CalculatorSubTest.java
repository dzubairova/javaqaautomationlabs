package hw1;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorSubTest extends AbstractCalculatorTest{
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Long subtraction",
			groups = {"add and subtract"})
	public void longSubTest(long a, long b, long expected) {
		long actual = calculator.sub(a, b);
		assertEquals(expected, actual);
	}
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Double subtraction",
			groups = {"add and subtract"})
	public void doubleSubTest(double a, double b, double expected) {
		double actual = calculator.sub(a, b);
		assertEquals(expected, actual);
	}
}
