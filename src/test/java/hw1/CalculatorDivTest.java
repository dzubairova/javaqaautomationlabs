package hw1;

import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;

public class CalculatorDivTest extends AbstractCalculatorTest {
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Long division")
	public void longDivTest(long a, int b, long expected) {
		long actual = calculator.div(a, b);
		assertEquals(expected, actual);
	}
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Double division",
			groups = {"multiply and divide"})
	public void doubleDivTest(double a, double b, double expected) {
		double actual = calculator.div(a, b);
		assertEquals(expected, actual);
	}
	
	@Test(expectedExceptions = NumberFormatException.class,
			expectedExceptionsMessageRegExp = "Attempt to divide by zero",
			groups = {"multiply and divide"})
	public void throwExceptionOnZeroDivision() {
		calculator.div(10, 0);
	}
}
