package hw1;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorMultTest extends AbstractCalculatorTest {
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Long multiplication",
	groups = {"multiply and divide"})
	public void longMultTest(long a, int b, long expected) {
		long actual = calculator.mult(a, b);
		assertEquals(expected, actual);
	}
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Double multiplication",
			groups = {"multiply and divide"},
			enabled = false)
	public void doubleMultTest(double a, double b, double expected) {
		double actual = calculator.mult(a, b);
		assertEquals(expected, actual);
	}
}
