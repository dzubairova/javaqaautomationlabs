package hw1;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorIsPositiveTest extends AbstractCalculatorTest {
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "isPositive")
	public void isPositiveTest(long a, boolean expected) {
		boolean actual = calculator.isPositive(a);
		assertEquals(expected, actual);
	}
}
