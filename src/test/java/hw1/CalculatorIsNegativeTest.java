package hw1;

import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;

public class CalculatorIsNegativeTest extends AbstractCalculatorTest{
	
		@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "isNegative")
		public void isPositiveTest(long a, boolean expected) {
			boolean actual = calculator.isNegative(a);
			assertEquals(expected, actual);
		}
}
