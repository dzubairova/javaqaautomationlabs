package hw1;

import org.testng.annotations.DataProvider;

public class CalculatorDataProviders {
	
	@DataProvider(name ="Long sum")
	public static Object[][] longSumData() {
		return new Object[][]{
				{10, 20, 30},
				{-101, 102, 1},
				{31, 90000, 90031}
		};
	}
	
	@DataProvider(name ="Double sum")
	public static Object[][] doubleSumData() {
		return new Object[][]{
				{10.5, 2, 12.5},
				{100.3, 4.2, 104.5},
				{31.5, 111500.2, 111531.7}
		};
	}
	
	@DataProvider(name ="Long subtraction")
	public static Object[][] longSubData() {
		return new Object[][]{
				{5001, 2, 4999},
				{20_000, 200_500, -180_500},
				{7, 3, 4}
		};
	}
	
	@DataProvider(name ="Double subtraction")
	public static Object[][] doubleSubData() {
		return new Object[][]{
				{500, 2.5, 497.5},
				{200500.7, 20500.7, 180000.0},
				{7.7, 3.3, 4.4}
		};
	}
	
	@DataProvider(name ="Long multiplication")
	public static Object[][] longMultData() {
		return new Object[][]{
				{5001, 2, 10002},
				{20_000, 200_500, 4_010_000_000L},
				{7, 3, 21}
		};
	}
	
	@DataProvider(name ="Double multiplication")
	public static Object[][] doubleMultData() {
		return new Object[][]{
				{500, 2.5, 1250.0},
				{20.5, 20.5, 420.25},
				{7.2, 2.2, 15.0}
		};
	}
	
	@DataProvider(name ="Long division")
	public static Object[][] longDivData() {
		return new Object[][]{
				{500, 2, 250},
				{20000, 100, 200},
				{3600, 6, 600}
		};
	}
	
	@DataProvider(name ="Double division")
	public static Object[][] doubleDivData() {
		return new Object[][]{
				{22.2, 2, 11.1},
				{555.0, 2, 277.5},
				{100000000.0, 100000000.0, 1}
		};
	}
	
	@DataProvider(name ="Power")
	public static Object[][] doublePowData() {
		return new Object[][]{
				{2, 2, 4},
				{5, 3, 125},
				{2, 10, 1024}
		};
	}
	
	@DataProvider(name ="isPositive")
	public static Object[][] isPositiveData() {
		return new Object[][]{
				{2, true},
				{20_000_000, true},
				{0, false},
				{-1, false}
		};
	}
	
	@DataProvider(name ="isNegative")
	public static Object[][] isNegativeData() {
		return new Object[][]{
				{-1, true},
				{-20_000_000, true},
				{0, false},
				{1, false}
		};
	}
	
}
