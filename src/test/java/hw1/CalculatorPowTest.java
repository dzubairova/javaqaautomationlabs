package hw1;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorPowTest extends AbstractCalculatorTest{
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Power")
	public void powTest(double a, double b, double expected) {
		double actual = calculator.pow(a, b);
		assertEquals(expected, actual);
	}
}
