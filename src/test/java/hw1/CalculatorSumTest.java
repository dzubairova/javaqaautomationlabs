package hw1;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CalculatorSumTest extends AbstractCalculatorTest {
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Long sum",
			groups = {"add and subtract"})
	public void longSumTest(long a, int b, long expected) {
		long actual = calculator.sum(a, b);
		assertEquals(expected, actual);
	}
	
	@Test(dataProviderClass = CalculatorDataProviders.class, dataProvider = "Double sum",
			groups = {"add and subtract"})
	public void doubleSumTest(double a, double b, double expected) {
		double actual = calculator.sum(a, b);
		assertEquals(expected, actual);
	}
	
}
